package com.shaon2017.atifanisproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.button1)
    public void calculateBills() {
        startActivity(new Intent(this, CurrentReadingAcitivty.class));
    }

    @OnClick(R.id.button2)
    public void records() {
        startActivity(new Intent(this, RecordActivity.class));
    }



}

/*
 * Created by Shaon on 8/15/16 8:06 PM
 * Copyright (c) 2016. This is free to use in any software.
 * You must provide developer name on your project.
 */

package com.shaon2017.atifanisproject.service;


import com.shaon2017.atifanisproject.model.Reading;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Shaon on 8/15/2016.
 */
public interface APIService {

    @GET("atif_project/getPreviousReading.php")
    Call<Reading> getPreviousReading();

    @FormUrlEncoded
    @POST("atif_project/insertDataIntoDB.php")
    Call<Reading> saveDataIntoServerDB(@Field("reading") Double currentReading,
                                       @Field("date") String date);

    @GET("atif_project/getAllData.php")
    Call<List<Reading>> getAllData();
}

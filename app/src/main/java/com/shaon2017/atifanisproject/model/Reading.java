package com.shaon2017.atifanisproject.model;

/**
 * Created by Shaon on 6/30/2017.
 */

public class Reading {
    private int id;
    private String date;
    private double reading;

    public String getDate() {
        return date;
    }

    public double getReading() {
        return reading;
    }

    public int getId() {
        return id;
    }
}

package com.shaon2017.atifanisproject;

import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.shaon2017.atifanisproject.adapter.RecylcerViewAdapter;
import com.shaon2017.atifanisproject.model.Reading;
import com.shaon2017.atifanisproject.network.ApiClinet;
import com.shaon2017.atifanisproject.service.APIService;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Shaon on 7/1/2017.
 */

public class RecordActivity extends AppCompatActivity {

    private LinearLayoutManager lLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        ButterKnife.bind(this);

        if(isNetworkConnected()) {
            getAllData();
        }else
            Toast.makeText(this, "Please turn on your internet" +
                    " before calculation", Toast.LENGTH_LONG).show();

        lLayout = new LinearLayoutManager(this);

    }

    private void getAllData() {
        try {
            APIService service = ApiClinet.getClient().create(APIService.class);

            Call<List<Reading>> call = service.getAllData();

            call.enqueue(new Callback<List<Reading>>() {
                @Override
                public void onResponse(Call<List<Reading>> call, Response<List<Reading>> response) {
                    List<Reading> data = response.body();

                    RecyclerView rView = (RecyclerView)
                            findViewById(R.id.recyclerView);
                    rView.setLayoutManager(lLayout);
                    RecylcerViewAdapter rcAdapter = new RecylcerViewAdapter(data);
                    rView.setAdapter(rcAdapter);

                }

                @Override
                public void onFailure(Call<List<Reading>> call, Throwable t) {

                }
            });


        } catch (Exception e) {
            Log.d("onResponse", "There is an error");
            e.printStackTrace();

        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(MainActivity.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}

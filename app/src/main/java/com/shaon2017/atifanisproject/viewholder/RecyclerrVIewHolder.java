package com.shaon2017.atifanisproject.viewholder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.shaon2017.atifanisproject.R;
import com.shaon2017.atifanisproject.model.Reading;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Shaon on 6/6/2017.
 */

public class RecyclerrVIewHolder extends RecyclerView.ViewHolder {


    private List<Reading> itemList;

    public TextView recordID, recordDate, recordReading;

    public RecyclerrVIewHolder(View itemView,
                               List<Reading> itemList) {
        super(itemView);

        recordID = (TextView) itemView.findViewById(R.id.textRecordID);
        recordDate = (TextView) itemView.findViewById(R.id.textRecordDate);
        recordReading = (TextView) itemView.findViewById(R.id.textRecordReading);

        this.itemList = itemList;
    }


}

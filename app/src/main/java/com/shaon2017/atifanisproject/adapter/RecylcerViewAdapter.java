package com.shaon2017.atifanisproject.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.shaon2017.atifanisproject.R;
import com.shaon2017.atifanisproject.model.Reading;
import com.shaon2017.atifanisproject.viewholder.RecyclerrVIewHolder;

import java.util.List;

/**
 * Created by Shaon on 6/6/2017.
 */

public class RecylcerViewAdapter extends RecyclerView.Adapter<RecyclerrVIewHolder> {
    private List<Reading> itemList;



    public RecylcerViewAdapter(List<Reading> itemList) {
        this.itemList = itemList;

    }

    @Override
    public RecyclerrVIewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, null);

        RecyclerrVIewHolder rcv = new RecyclerrVIewHolder(layoutView, itemList);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerrVIewHolder holder, int position) {
        holder.recordID.setText("" + itemList.get(position).getId());
        holder.recordDate.setText("" + itemList.get(position).getDate());
        holder.recordReading.setText("" + itemList.get(position).getReading());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}

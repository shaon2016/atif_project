package com.shaon2017.atifanisproject;

import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.shaon2017.atifanisproject.model.Reading;
import com.shaon2017.atifanisproject.network.ApiClinet;
import com.shaon2017.atifanisproject.service.APIService;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Shaon on 6/25/2017.
 */

public class CurrentReadingAcitivty extends AppCompatActivity {
    @BindView(R.id.editCurrentReading)
    EditText editCurrentReading;

    @BindView(R.id.textCurrentReading)
    TextView textCurrentReading;
    @BindView(R.id.textPreviousReading)
    TextView textPreviousReading;
    @BindView(R.id.textConsumedUnit)
    TextView textConsumedUnit;
    @BindView(R.id.textBill)
    TextView textBill;

    double prevReading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_reading);

        ButterKnife.bind(this);

        if(isNetworkConnected()) {
            getPrevReading();

        }else
            Toast.makeText(this, "Please turn on your internet" +
                    " before calculation", Toast.LENGTH_LONG).show();

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(MainActivity.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void getPrevReading() {
        try {
            APIService service = ApiClinet.getClient().create(APIService.class);



            Call<Reading> call = service.getPreviousReading();


            call.enqueue(new Callback<Reading>() {
                @Override
                public void onResponse(Call<Reading> call, Response<Reading> response) {
                    prevReading = response.body().getReading();

                }

                @Override
                public void onFailure(Call<Reading> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Log.d("onResponse", "There is an error");
            e.printStackTrace();

        }
    }
    
    @OnClick(R.id.btnCalculate)
    public void calculate() {
        Double currentReading = Double.valueOf(editCurrentReading.getText().toString());

        double consumedUnit, bill = 0;

        consumedUnit = currentReading - prevReading;

        if(consumedUnit >= 1 && consumedUnit <= 50) {
            bill = consumedUnit * 3.87;
        }else if(consumedUnit >= 51 && consumedUnit <= 75) {
            bill = consumedUnit * 3.80;
        }else if(consumedUnit >= 76 && consumedUnit <= 200) {
            bill = consumedUnit * 5.14;
        }else if(consumedUnit >= 201 && consumedUnit <= 300) {
            bill = consumedUnit * 5.36;
        }else if(consumedUnit >= 301 && consumedUnit <= 400) {
            bill = consumedUnit * 5.63;
        }else if(consumedUnit >= 401 && consumedUnit <= 600) {
            bill = consumedUnit * 8.70;
        }else if(consumedUnit >= 600) {
            bill = consumedUnit * 9.98;
        }

        textCurrentReading.setText("" + currentReading);
        textPreviousReading.setText("" + prevReading);
        textConsumedUnit.setText("" + consumedUnit);
        textBill.setText("" + bill);

        saveDataIntoServerDB(currentReading);
    }

    private void saveDataIntoServerDB(Double currentReading) {
        try {
            APIService service = ApiClinet.getClient().create(APIService.class);

            String date = getCurrentDate();

            Call<Reading> call = service.saveDataIntoServerDB(currentReading, date);


            call.enqueue(new Callback<Reading>() {
                @Override
                public void onResponse(Call<Reading> call, Response<Reading> response) {

                }

                @Override
                public void onFailure(Call<Reading> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Log.d("onResponse", "There is an error");
            e.printStackTrace();

        }
    }

    private String getCurrentDate() {
        String dateString = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        dateString = simpleDateFormat.format(date);

        return dateString;
    }
}

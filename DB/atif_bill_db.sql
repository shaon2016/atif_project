-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 30, 2017 at 04:27 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shaoniiu_otherapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `atif_bill_db`
--

CREATE TABLE `atif_bill_db` (
  `id` int(11) NOT NULL,
  `date` varchar(30) DEFAULT NULL,
  `reading` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atif_bill_db`
--

INSERT INTO `atif_bill_db` (`id`, `date`, `reading`) VALUES
(1, '15-06-2017', 150),
(2, '30-06-2017', 180),
(3, '31-06-2017', 200);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atif_bill_db`
--
ALTER TABLE `atif_bill_db`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atif_bill_db`
--
ALTER TABLE `atif_bill_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
